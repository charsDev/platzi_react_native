# Introduccion al curso

## __El porder de React Native__
* __React Native__ Crea aplicaciones moviles nativas para Android y iOS usando Javascript y React
* El __componente View__ de native funciona como un div mientras que el __componente Text__ funciona como un párrafo.
* Componentes principales de react native
  * View / ScrollView / SafeAreaView (Divs, scrolls)
  * Text / TextInput (Formulario)
  * Image / ImageBackground (Imágenes)
  * FlatList / SectionList (Listas de elementos)
  * Touchable Higligth / Opacy / WithoutFeedback (Componentes para manejar los taps del teléfono)
  * Animated (Sirve para animar la interfaces)
  * fetch, async await, sockets (Funciones asíncronas)
  * Platform (Permite identificar si es iOS o Android)
  * AsyncStorage (Equivalente a localStorage)
* __¿Cuándo usar React Native?__
  * La velocidad de desarrollo es muy importante para un equipo
* __¿Hace falta aprender Java y objetive-c?__
  * Dile NO a la pereza. 
  * No limites aprender una tecnología por saber otra.
* __¿Cómo funciona?__
  * React Bridge. “Convierte” el código js a código nativo.
* __Developer Experience__
  * Hot/live reloading
  * Depuración de Javascript
  * Network inspector
  * Stack Trace
* __¿Quiénes usan react native?__
  * Uber eats
  * Instagram
  * Facebook
  * Wix
  * Skype
  * Pinterest
  * Platzi

## __Proyecto nuevo en React Native y creacion del emulador en Android Studio(AS)__
* Instalamos el cli de React Native __npm install -g react-native-cli__
* Iniciamos un proyecto con __react-native init platziVideoApp__
  * Este nos crea la carpeta __platziVideoApp__ que contiene todo lo necesario para poder usar React Native
  * Cambiamos la version de react-native en el package.json y la del babel-preset-env
* En __Android Studio__, previamente configurado (detalles en la platzi)
  * Abrimos la carpeta de nuestro proyecto que creamos anteriormente __platziVideoApp__
  * Si utilizamos el emulador:
    * Hacemos click en el boton play de color verde
    * Elegimos o creamos el AVD
    * Esperamos a que se muestre el emulador
    * Despues vamos a la consola(cmd) a la direccion de la carpeta
    * Corremos el comando react-native run-android(en este caso)
  * Si lo hacemos conectando un telefono
    * Ponemos el telefono en modo desarrollador, depurar por usb
    * Conectamos el telefono al pc
    * Corremos Android Studio
    * Abrimos consoloa y checamos que reconozca nuestro equipo tecleand adb devices
    * Usamos el comando react-native run-android

# Componentes y APIs de React Native

## __Componentes en React Native__
* El framework de __React Native__ nos provee de componentes para crear interfaces
* Los componentes principales (Leonidas) son:
  * Vistas(Views) = div, aside, section, span, etc
  * Textos(Text) = p, h1..., etc
  * Imagenes(Image) = img
* En el componente principal __App.js__, este fue creado al hacer reac-native init
  * Importo __Platform, StyleSheet, Text, View__ desde react-native
  * Retornamos tres componentes de __Text__ dentro del componente __View__, cada uno de ellos con sus respectivos estilos
  * Importamos un nuevo componente __Image__ desde react-native
  * Utilizamos el componente __Image__ para mostrar una imagen
    * require = para imagenes en el directorio
    * uri = para imagenes desde algun servidor, pagina
    * style = para estilos, en este caso aqui mismo las declaramos, pero podemos declararlas aparte como en los casos de los Text y View

## __Estilos en React Native__
* Para utilizar estilos en __React Native__ utilizamos __StyleSheet__
* En el componenten __App.js__:
  * Importamos __StyleSheet__ desde react-native
  * Para utilizar los estilos, dentro de los componentes llamamos la propiedad __style__ la cual sera igual a un objeto apuntando a una de sus llaves. En este caso el objeto se llama __styles__
  * Declaramos la constante __styles__ con el metodo __StyleSheet.create__ creamos un objeto y le pasamos los objetos de los estilos que declaramos en el componente y dentro de este declaramos los estilos como lo hacemos en css
  * OJO los nombres de los estilos cambian, no existe la opcion de usar - , debido a que es JS debemos usar camelCase
  * En los estilos podemos encontrar flex-box, pero aqui es conocido como __flex__ en el cual los contenedores se alinean de manera vertical, no como en web que es horizontal
  * Si quisieramos que la app se comportara segun el sistema donde la corramos, debemos utilizar el metodo __Platform.select__ que crea un objeto con dos keys:
    * __ios__: para el sistema operativo apple
    * __android__: sistema operativo android
  * En los estilos del __container__
    * Cambiamos el __backgroundColor__ de tal manera que si la app se corre en __ios__ mande un color verde de fondo y si es en __Android__ sera un color azul. Esto con __Platform.select__
* Si presionamos __ctrl + m__ veremos unas opciones de las cuales explicaremos dos:
  * __Live Reload__: Recarga la app pero se ve un pantallazo blanco cuando hace el refresh 
  * __Hot Reload__: Al igual que el anterior solo que en esta opcion no se ve el pantallazo

## __Organizacion del proyecto__
* Creamos la carpeta __src__ y dentro agregamos las carpetas __player__, __screens__, __sections__ y __videos__ cada una con sus carpetas __components__ y __containers__
  * __screens__: aqui guardaremos las pantallas mas basicas de nuestra app, en este caso son dos: uno de la home y otro donde vamos a ver las peliculas.Por convencion todos deben ser containers.

* En el componente __containers/home.js__:
  * Importamos __React__ y __Component__ debido a que este sera un smart component
  * Creamos la clase __Home__ y retornamos los __this.props.childrens__ esto se refiere a que renderizara los componentes que esten dentro de este componente cuando sea llamado en el componente principal __App.js__
  *OJO este componente tienen la estrucutura basica de los componentes de React*

* En el componente __App.js__:
  * Eliminamos todo el contenido que viene con react native al crear la app
  * Solo dejamos __Text__ en los imports de react-native
  * Importamos el componente __Home__
  * En la clase __App__:
    * Retornamos el componente __Home__ que dentro lleva los componentes __Text__ con los nombres de las secciones las cuales son la estructura del proyecto
  *OJO como no me gusta el export default pegado a la class, decidi mover el export default a la parte de abajo como se hace en React*

## __Soporte a iPhone X en React Native: Creación del Header__
* Creamos el componente __components/header.js__:
  * Importamos __React__ desde react
  * Importamos __View__, __Text__, __Image__, __StyleSheet__, __SafeAreaView__ desde react-native
  * Creamos una funcion __Header__:
    * Pasamos un componente __View__:
      * Un componente __SafeAreaView__ el cual nos sirve para que al momento de renderizar la app para ios no de problemas el notch y la barra de notificaciones.
      * Dentro un componente __Image__ con sus propiedades entre ellos los estilos.
      * Otro componente __View__ el cual tiene sus estilos y dentro pasara los hijos que vengan por props __props.children__. 
      * *A esto se le conoce como composicion de componentes*
  * Declaramos la const __styles__ la cual utiliza el metodo __StyleSheet.create__
    * Creamos una llave __logo__ en la cual declaramos los estilos:
      * __resizeMode__: es el background-size del css, en default esta en cover.
      * __paddingVertical__: para hacer un padding de manera vertical
      * __paddingHorizontal__: padding horizontal
      * Las opciones de flex

* En el componente __App.js__:
  * Importamos nuestro componente __Header__
  * Dentro del componente __Home__ pasamos nuestro componente __Header__, que sustituye al Text que hacia referencia al header.

## __Listas en React Native__
* React Native tiene dos componentes para las listas:
 * __FlatList__: Listas sencillas
 * __SectionList__: Lista anidadas, complejas

* Creamos el componente __containers/suggestion-list.js__:
  * Importamos __FlatList__ y __Text__
  * Creamos la clase __SuggestionList__:
    * Para efectos del ejercicio creamos una constante list
    * Retornamos un componente __FlatList__ con sus propiedades:
      * __data__: la cual sera igual a los datos a mostrar en la lista
      * __renderItem__: es igual a una arrow function, que recibe como parametro toda la lista, en automatico, entonces sacamos el item de cada elemento de la lsita con el __{ item }__ como parametro de la arrow function. Retornamos un componente __Text__ que mostrara el title de cada item.

* En el componente __App.js__
  * Importamos el componente __SuggestionList__
  * Lo agregamos a los componentes dentro del componente __Home__

## __Estructura de recomendaciones__
* Se crea el componente __suggestion-list-layout.js__
  * Se importan dependecias a utilizar
  * Se crea la arrow function __SuggestionListLayout__
    * Una etiqueta __View__ , con sus estilos, que renderiza lo que venga como props.children (composicion de componentes)
      * Una etiqueta __Text__ , con sus estilos, que renderiza la props.title

 * En el componente __suggestion-list.js__
  * Importamos __Layout__ desde el componente __suggestion-list-layout.js__
  * En el return usamos el componente __Layout__, le pasamos la propiedad __title__
    * Dentro de este agreagamos el __FlatList__

*Hicimos esto para poder poner un titulo y estilos. Por buenas practicas los estilos no deben estar dentro de un smart component, por eso creamos el componente Layout*

## __Separadores y listas vacías__
* En el componente __suggestion-list.js__
  * Importamos los componentes __Empty__ y __Separator__
  * Creamos dos funciones __renderEmpty__ e __itemSeparator__ las cuales retornan los componentes importados
  * En la constante __list__ agregamos datos
  * En el componente __FlatList__ pasamos como propiedades:
    * __ListEmptyComponent__: el cual llama a la funcion __renderEmpty__. Esta propiedad retorna el valor de los datos de la lista si tiene y si no hay valor en la lista retorna un texto, el cual pasamos como propiedad en el componente __Empty__ que retornamos en la funcion __renderEmpty__
    * __ItemSeparatorComponent__: el cual nos servira como un separador para las partes de nuestro diseño. El diseño y estilos del separador los declaramos en el componente __Separator__ que retornamos en la funcion __itemSeparator__

* Creamos el componente __empty.js__
  * Hacemos los importes necesarios
  * Creamos una funcion __Empty__
    * Dentro usamos el componente __View__ el cual dentro tiene el componente __Text__ ,ambos con sus estilos, que renderiza los props.text
  * Creamos los estilos en la constate __styles__

* Creamos el componente __vertical-separator.js__
  * Importamos las dependecias necesarias
  * Declaramos la funcion __VerticalSeparator__:
    * Dentro un componete __View__ el cual tiene su propiedad __style__ la cual recibe los estilos de styles.separator y una condicional si recibe props.color como parametro (en el componente suggestion-list.js) y si no que pinte el color hexadecimal por default (#eaeaea)
    * Dentro del __View__ declaramos el componente __Text__ con un texto
  * Declaramos los estilos en la constante __styles__

## __Componente de sugerencia__
* En el componente __suggestion-list.js__
  * Importamos el componente __Suggestion__
  * Creamos la funcion __renderItem__ la cual devuelve el contenido del componente __Suggestion__ por cada item en la const list, este componente recibe el array de los items *spread operator ...item*
  * En el componente __FlatList__ llamamos a la funcion __renderItem__

* Creamos el componente __suggestion.js__
  * Importamos las dependemcias necesarias
  * Creamos la funcion __Suggestion__
    * Renderiza el contendio de nuestro componente en las etiquetas __View__, __Image__ y __Text__ con sus estilos
  * Creamos la const styles que contiene los estilos

## __Haciendo peticiones a un servidor (fetch)__
* Creamos el archivo __api.js__
  * Declaramos una const __BASE_API__ , esta es la direccion base para consultar la API
  * Creamos una clase __Api__ 
    * Dentro creamos la funcion asincrona __getSuggestion__ la cual se encargara de traer los datos de la API
    *En la constante data, encerramos entre {} la data, ya que esa es la llave que nos interesa de todas las que trae la API, de otra forma tendriamos que navegar por todo el json para llegar a ella*
  * Exportamos la clase y la instanciamos(new)

* En el componente __App.js__
  * Importamos __API__
  * Llamamos asincronamente al __componentDidMount__
    * Dentro en la constante __movies__ llamamos a la clase __API__ con su metodo __getSuggestion__ y le pasamos como parametro el 10, que seria el id

*En el emulador si presionamos ctrl + m, podemos hacer debbuging en chrome si seleccionamos Remote JS Debugging*

## __Integrando datos traídos del API al proyecto__
* En el componente __suggestion-list.js__
  * Creamos la funcion __keyExtractor__ la cual recibe un item como parametro y retorna el id del item convertido a string
  * Llamamos a la propiedad __keyExtractor__ del componente FlatList la cual hace referencia a la funcion con el mismo nombre declarada anteriormente.

* En el componente __suggestion.js__
  * En la funcion __Suggestion__ le pasamos como parametros el props
  * Cambiamos los datos hardcodeados por los que recibe ahora por props

* En el componente __App.js__
  * Declaramos el __state__ inicial con la key __suggestionList__ igual a []
  * En el __componentDidMount__ seteamos el state suggestionList con el valor de la constante movies
  * En el componente __SuggestionList__ cambiamos el valor del parametro list y le asignamos el valor del state.suggestionList

* En el componente __veritcal-separator.js__
  * Eliminamos un componente __Text__ que estaba de mas

## __Lista de Categorías__
* En el archivo __api.js__
  * Creamos una nueva funcion asincrona __getMovies__ la cual nos retorna una lista de peliculas

* Creamos el componente __category_list.js__
  * Este componente es igual al componente __suggestion_list.js__ creado en la lección anterior. Es por esto que hicimos un copy paste de este.

* En el componente __App.js__
  * Importamos el componente __CategoryList__
  * Agregamos un nuevo state __categoryList__
  * En la funcion asincrona __componentDidMount__
    * Agregamos la constante __categories__ que nos devuelve el contenido de __API.getMovies__ 
    * Seteamos el state categoryList con el valor de categories
  * Retornamos el componente __CategoryList__
    * Le pasamos la propiedad __list__ que es igual al valor de this.state.categoryList

## __Separadores Horizontales__
* En el componente __category-list.js__
  * Cambiamos la ruta del import __Separator__ para que apunte a nuestro componente para seapradores horizontales
  * Importamos el componente __Layout__
  * Envolvemos el componente FlatList dentro del componente __Layout__ y le pasamos la propiedad __title__

* Creamos el componente __category-list-layout.js__
  * Este componente es casi igual al componente __suggestion-list-layout.js__
  * Utilizamos el componente __ImageBackground__ para poner la imagen de fondo
  * Modificamos los estilos

* Creamos el componente __horizontal-separator.js__
  * Este componente es muy pareciod al componente __vertical-separator.js__, solo que con sus respectivos cambios correspondientes

## __Componente de Categoria__
* En el componente __category-list.js__
  * Cambiamos el importe de __Separator__ por __Category__
  * En la funcion __renderItem__ cambiamos el nombre del componeten __Category__ que retorna la funcion

* Creamos el componente __category.js__
  * Importamos las dependecias necesarias
  * Creamos la funcion __Category__ la cual retorna 
    * Un componente __ImageBackground__ con sus propiedades, el cual envuelve al compontente __Text__ con sus propiedades
  * Creamos la constante __styles__ y dentro los estilos

## __Instalando plugins de la comunidad__
* Instalamos el plugin [__react-native-video__](https://github.com/react-native-community/react-native-video)
* Debido a que este plugin es desarollado en lenguajes nativos para ios y android es necesarion linkearlo con nuestro proyecto, esto lo hacemos con el comnado __link__ de la siguiente forma __react-native link react-native-video__
* En el componente __App.js__
  * Instalamos la dependecia __Video__ desde react-native-video
  * En el return agregamos el componente __Video__ dentro de un componente __View__ y le pasamos estilos y en el componeten Video agregamos la propiedad Source

*En el link del plugin vienen mas propiedades de este. Se uso la version 3.2.1 de react-native-video, para que esta funcione y no se cierre se debe utilizar la API 27 de Androisd Studio*

## __Creando un reproductor de video__
* Creamos el componente __player.js__
  * Importamos dependencias
  * Creamos la clase __Player__ la cual retornara un componente __Layout__ el cual tendra la propiedad video que sera igual a el componente __Video__ de react-native-video con sus propiedades

* Creamos el componente __video-layout.js__
  * Importamos dependencias
  * Creamos la funcion __Layout__ que retorna un componente View que contiene otro componente View el cual renderiza la props.video. Con sus estilos
  * Creamos los estilos

## __Controlando el buffer del video__
* En el componente __player.js__
  * Importamosel componente __ActivityIndicator__ desde react-native. Este nos sirve para dibujar el icono animado de espera en lo que carga el video.
  * Declaramos un __state.loading__ que sera igual a true
  * Creamos la funcion __onBuffer__ que recibe como parametro __isBuffering__ el cual nos indica si esta cargando(true) y si no(false)
  * Creamos la funcion __onLoad__ la cual nos ayudara a quitar el icono animado de carga, ya que en el emulador de Android no se quitaba con el el onBuffer
  * Al componente __Layout__ agregamos dos nuevas propiedades 
    * __loading__ que sera igual al state.loading
    * __loader__ que es igual al componente __ActivityIndicator__ y le indicamos de que color sera este indicador.
  * En el componente __Video__ agregamos los parametros __onBuffer__ y __onLoad__ que seran igual a las funciones que llevan el mismo nombre, respectivamente.

* En el componente __video-layout.js__
  * En la funcion __Layout__ agregamos una etiqueta __View__ con sus esitlos y renderiza __props.loader__ si __props.loading__ es igual a true
  * Agregamos los estilos

## __Controles del reproductor__
* Debido a que las categorias estaba mandando un error, en el componente __category.js__ hizimos una validacion para que cuando reciba props.genre muestre la primera, pero si no, muestre el texo 'No hay categorias'

* En el componente __player.js__
  * Importamos los componentes requeridos
  * Agregamos una nueva key al state
  * Creamos la funcion __playPause__ la cual cambia el state.pause a el valor contrario que tenga en ese momento
  * En el componente __Video__ agregamos la propiedad __pause__ esta por default tiene el valor de false
  * En el componente __Layout__ anexamos la propiedad __controls__ la cual sera igual a el componente __ControlLayout__ que asu vez contendra al componente __PlayPause__ con sus propiedades:
    * __onPress__: que hace referencia a la funcion playPause
    * __paused__: que es igual al valor de this.state.paused
  * Tambien contiene componentes __Text__ para hacer referencia a lo que hace falta

* En el componente __video-layout.js__
  * En la funcion __Layout__ renderizamos las props.controls

* __TouchableHighlight__, al mantener presionado el componente cambia de color.

* __TouchableOpacity__, al mantener presionado, el componente se hace un poco transparente (baja su opacidad).

* __TouchableWithoutFeedback__, No tiene efectos visuales al mantener presionado el botón.

* La propiedad __onPress__ para realizar eventos en ReactNative solo funciona con los componentes Touchable.

* La propiedad __underlayColor__ cambia el color que se va a cambiar al mantener presionado el componente TouchableHighlight.

* Creamos el componente __play-pause.js__
  * Impotamos dependecias y componentes
  * Creamos funcion __PlayPause__ 
    * Retornamos el componente __TouchableHighlight__ con sus propiedades:
      * __onPress__: que viene en las props desde player.js
      * __style__: estilos
      * __underlayColor__: propiedad para cambiar el color al presionar el componente
      * __hitSlop__: propiedad para agrandar el area para presionar el componente
    * Dentro del componente creamos una condicional (props.paused)
    * Creamos estilos

* Creamos el componente __control-layout.js__
  * Importamos dependecias y componentes
  * Creamos funcion __ControlLayout__ devuelve un componente __View__ on sus estilos y dentro los porps.children
  * Creamos estilos